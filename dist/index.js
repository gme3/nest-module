
'use strict'

if (process.env.NODE_ENV === 'production') {
  module.exports = require('./user-manual-backend.cjs.production.min.js')
} else {
  module.exports = require('./user-manual-backend.cjs.development.js')
}
