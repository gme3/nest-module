import { Response } from 'express';
import { UserManualService } from './user-manual.service';
export declare class UserManualController {
    private userManualService;
    constructor(userManualService: UserManualService);
    getList(): Promise<any[]>;
    getDocument(response: Response, path: string): Promise<Response>;
}
