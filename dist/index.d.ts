import { UserManualController } from './user-manual.controller';
import { UserManualService } from './user-manual.service';
import { UserManualModule } from './user-manual.module';
export { UserManualModule, UserManualService, UserManualController };
export declare const roles: String[];
