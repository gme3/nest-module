import { ExternalStorageService } from '@private-npm/nestjs-packages';
import { Response } from 'express';
export declare class UserManualService {
    private readonly externalStorageService;
    constructor(externalStorageService: ExternalStorageService);
    getManualsList(): Promise<any[]>;
    getManualDocument(response: Response, path: string): Promise<Response>;
}
