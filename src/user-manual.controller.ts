import {  Controller,
  Get,
  Res,
  Query,
  BadRequestException,
} from '@nestjs/common';
import { Response } from 'express';
import { UserManualService } from './user-manual.service';

@Controller('user-manual')
export class UserManualController {
  constructor(private userManualService : UserManualService) { }

  @Get()
  async getList(): Promise<any[]> {
    console.log("trying to provide list from um service")
    return await this.userManualService.getManualsList();
  }

  @Get('document')
  async getDocument(
    @Res() response: Response,
    @Query('path') path: string,
  ): Promise<Response> {
    if (path === undefined) {
      throw new BadRequestException();
    }
    return await this.userManualService.getManualDocument(response, path);
  }
}
