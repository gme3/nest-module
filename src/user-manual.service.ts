import { Injectable, Inject, forwardRef } from '@nestjs/common';
import { ExternalStorageService } from '@private-npm/nestjs-packages';
import { Response } from 'express';

const BUCKET = "manuals"

@Injectable()
export class UserManualService {
  constructor(
    @Inject(forwardRef(() => ExternalStorageService))
    private readonly externalStorageService: ExternalStorageService,
  ) {}

  getManualsList(): Promise<any[]> {
    console.log("Serving list from um service")
    return this.externalStorageService.getList('manuals');
  }

  getManualDocument(response: Response,path: string): Promise<Response> {
    return this.externalStorageService.getFile(response, BUCKET, path)
  }
}
