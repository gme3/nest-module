import { DynamicModule, Module } from '@nestjs/common';
import { UserManualController } from './user-manual.controller';
import { UserManualService } from './user-manual.service';

@Module({})
export class UserManualModule {
  static forRoot(): DynamicModule {
    return {
      controllers: [UserManualController],
      providers: [UserManualService],
      exports: [UserManualService],
      module: UserManualModule,
    };
  }
}