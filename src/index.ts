import { UserManualController } from './user-manual.controller'
import { UserManualService } from './user-manual.service'
import { UserManualModule } from './user-manual.module'

export { UserManualModule, UserManualService, UserManualController };
export const roles : String[] = [
  "_user-manual_view",
  "_user-manual_edit"
]